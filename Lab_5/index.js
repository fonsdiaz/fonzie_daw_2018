function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data)); 
}

document.getElementById("title").onmouseover = function() {mouseOver()};
document.getElementById("title").onmouseout = function() {mouseOut()};

function mouseOver() {
    document.getElementById("title").style.color = "red";
    document.getElementById("title").style.fontFamily = "Impact,Charcoal,sans-serif";
}

function mouseOut() {
    document.getElementById("title").style.color = "black";
    document.getElementById("title").innerHTML = "Lab 6";
}

function myFunction() {
    for (var i = 0; i <= 10; i++) {
    setInterval(function(){ alert("ha pasado "+i+" vez"); }, 5000);
    }
}

function instruc() {
    setTimeout(function(){ alert("Debes agarrar la imagen que esta a la izquierda y arrastrarla a el recuadro de arriba"); }, 100);
}