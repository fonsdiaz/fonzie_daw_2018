<?php

session_start();

if (isset($_POST['submit'])) {
	
	include 'dbh.php';

	$uid = mysqli_real_escape_string($conn, $_post['uid']);
	$pwd = mysqli_real_escape_string($conn, $_post['pwd']);

	//Error handlers
	//Check if inputs are empty

	if (empty($uid) || empty ($pwd)) {
		header("Location: ../index.php?login=empty");
		exit();
	} else {
		$sql = "SELECT * FROM users WHERE user_uid = '$uid' OR user_email = '$uid'";
		$result = mysqli_query($conn, $sql);
		$resultCheck = mysqli_num_rows($result);
		if ($resultCheck < 1) {
			header("Location: ../index.php?login=error");
			exit();
		} else {
			if ($row = mysqli_fetch_assoc($result)) {
				//De-hashing the pwd
				$hashedPwdCheck = password_verify($pwd, $row['user_pwd']);
				if ($hashedPwdCheck == false) {
					header("Location: ../index.php?login=error");
					exit();
				} elseif ($hashedPwdCheck == true) {
					//Log in the user HERE 
					$_SESSIONN['u_id'] = $row['user_id'];
					$_SESSIONN['u_first'] = $row['user_first'];
					$_SESSIONN['u_last'] = $row['user_last'];
					$_SESSIONN['u_email'] = $row['user_email'];
					$_SESSIONN['u_uid'] = $row['user_uid'];
					header("Location: ../index.php?login=succes");
					exit();
				}
			}
		}
	} 

	} else {
		header("Location: ../index.php?login=error");
		exit();
	}
